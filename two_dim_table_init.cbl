       IDENTIFICATION DIVISION. 
       PROGRAM-ID. TWO-DIM-TABLE.
       AUTHOR. Mankhong.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  WS-VALUE  .
           05 FILLER PIC X(2) VALUE "01". 
           05 FILLER PIC X(2) VALUE "02". 
           05 FILLER PIC X(2) VALUE "03". 
           05 FILLER PIC X(2) VALUE "04". 
           05 FILLER PIC X(2) VALUE "05". 
           05 FILLER PIC X(2) VALUE "06". 
           05 FILLER PIC X(2) VALUE "07". 
           05 FILLER PIC X(2) VALUE "08". 
           05 FILLER PIC X(2) VALUE "09".            
       01  WS-TABLE REDEFINES WS-VALUE .
           05 WS-ROW OCCURS 3 TIMES.
              10 WS-COL PIC X(2) OCCURS 3 TIMES VALUE "-".
       01  WS-IDX-ROW  PIC 9.
       01  WS-IDX-COL  PIC 9.
       PROCEDURE DIVISION .

       BEGIN.
           DISPLAY WS-TABLE 
           PERFORM VARYING WS-IDX-ROW FROM 1 BY 1
              UNTIL WS-IDX-ROW > 3
              PERFORM VARYING WS-IDX-COL FROM 1 BY 1
              UNTIL WS-IDX-COL > 3
                 DISPLAY WS-COL (WS-IDX-ROW, WS-IDX-COL)
                 
               END-PERFORM
               
           END-PERFORM
       .